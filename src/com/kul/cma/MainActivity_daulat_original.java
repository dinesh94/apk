package com.kul.cma;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.test.R;

@SuppressLint("SetJavaScriptEnabled")
public class MainActivity_daulat_original extends Activity implements OnClickListener {

	private WebView webViewBrowser;
	private ImageView splashImageView;
	private View progressBar;
	Button loginbutton;
	Button watchLive;
	String prevURl;

	private ValueCallback<Uri> mUploadMessage;
	private final static int FILECHOOSER_RESULTCODE = 1;

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == FILECHOOSER_RESULTCODE) {
			if (null == mUploadMessage)
				return;
			Uri result = intent == null || resultCode != RESULT_OK ? null
					: intent.getData();
			mUploadMessage.onReceiveValue(result);
			mUploadMessage = null;

		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		webViewBrowser = (WebView) findViewById(R.id.webView);
		splashImageView = (ImageView) findViewById(R.id.splashImageView);
		loginbutton = new Button(MainActivity_daulat_original.this);
		loginbutton.setOnClickListener(this);
		watchLive = new Button(MainActivity_daulat_original.this);
		watchLive.setOnClickListener(this);

		progressBar = findViewById(R.id.progressBar);
		webViewBrowser.setWebViewClient(new MyBrowser(getApplicationContext()));
		 webViewBrowser.setWebChromeClient(new WebChromeClient());
		webViewBrowser.getSettings().setLoadsImagesAutomatically(true);
		webViewBrowser.getSettings().setJavaScriptEnabled(true);
		webViewBrowser.getSettings().setSupportMultipleWindows(true);
		 webViewBrowser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webViewBrowser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		 webViewBrowser.addJavascriptInterface(new
		 MyJavaScriptInterface(MainActivity_daulat_original.this), "HtmlViewer");
		 webViewBrowser.addJavascriptInterface(new
		 WebAppInterface(MainActivity_daulat_original.this), "Android");
		webViewBrowser.getSettings().setDomStorageEnabled(true);
		webViewBrowser.getSettings().setJavaScriptCanOpenWindowsAutomatically(
				true);

		// Enablejavascript
		WebSettings ws = webViewBrowser.getSettings();
		ws.setJavaScriptEnabled(true);
		ws.setBlockNetworkImage(false);
		
		 ws.setSupportZoom(false);
		    ws.setAllowFileAccess(true);
		    ws.setAllowFileAccess(true);
		    ws.setAllowContentAccess(true);
		// Add the interface to record javascript events
		webViewBrowser.addJavascriptInterface(loginbutton, "loginbutton");
		
		if (isConnectingToInternet())
			webViewBrowser.loadUrl(getString(R.string.load_url));
		else
			alert();

		// File Download

		webViewBrowser.setDownloadListener(new DownloadListener() {

			public void onDownloadStart(String url, String userAgent,
					String contentDisposition, String mimetype,
					long contentLength) {
				DownloadManager.Request request = new DownloadManager.Request(
						Uri.parse(url));

				request.allowScanningByMediaScanner();
				request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); // Notify
																												// client
																												// once
																												// download
																												// is
																												// completed!
				request.setDestinationInExternalPublicDir(
						Environment.DIRECTORY_DOWNLOADS, "Kul_Builder_image ");
				DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
				dm.enqueue(request);
				Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT); // This
																			// is
																			// important!
				intent.addCategory(Intent.CATEGORY_OPENABLE); // CATEGORY.OPENABLE
				intent.setType("*/*");// any application,any extension
				Toast.makeText(getApplicationContext(), "Downloading File", // To
																			// notify
																			// the
																			// Client
																			// that
																			// the
																			// file
																			// is
																			// being
																			// downloaded
						Toast.LENGTH_LONG).show();

			}
		});

		
		


		
		webViewBrowser.setWebChromeClient(new WebChromeClient() {
			@SuppressWarnings("unused")
			public void openFileChooser(ValueCallback<Uri> uploadMsg) {
				mUploadMessage = uploadMsg;
				Log.d("myfirstapp_tag",
						"1) MainActivity.java loaded openFileChooser");
				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				Log.d("myfirstapp_tag",
						"2) MainActivity.java loaded openFileChooser");
				i.addCategory(Intent.CATEGORY_OPENABLE);
				Log.d("myfirstapp_tag",
						"3) MainActivity.java loaded openFileChooser");
				i.setType("image/*");
				Log.d("myfirstapp_tag",
						"4) MainActivity.java loaded openFileChooser");
				// Toast.makeText(getApplicationContext(),
				// "Login",Toast.LENGTH_LONG).show();

				MainActivity_daulat_original.this.startActivityForResult(
						Intent.createChooser(i, "File Chooser"),
						FILECHOOSER_RESULTCODE);
				Log.d("myfirstapp_tag",
						"5) MainActivity.java loaded openFileChooser");
			}
		});

	}

	@Override
	public void onClick(View v) {
		if (v.equals(loginbutton)) {
			Toast.makeText(getApplicationContext(), "Login", Toast.LENGTH_LONG)
					.show();
		}
		if (v.equals(watchLive)) {
			Toast.makeText(getApplicationContext(), "Watch_Live",
					Toast.LENGTH_LONG).show();
		}

	}

	public class WebAppInterface {
		Context mContext;

		/** Instantiate the interface and set the context */
		WebAppInterface(Context c) {
			mContext = c;
		}

		/** Show a toast from the web page */
		@JavascriptInterface
		public void showToast(String toast) {
			//
		}
	}

	private class MyBrowser extends WebViewClient {
		private Context context;

		public MyBrowser(Context context) {
			this.context = context;
		}

		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i("Overer", url);
			boolean shouldOverride = false;
			 Toast.makeText(getApplicationContext(), "Hello="+url,
			 Toast.LENGTH_SHORT).show();
			if (url.contains("index.html")) {
				prevURl = url;
			} else if(url.contains("login.html")){
				prevURl = url;
			}else{
				prevURl = url;
			}
//New Application Start here..
			view.addJavascriptInterface(new Object() {
				@JavascriptInterface
				public void performClick() throws Exception {
					if (isAppInstalled("mobileeye_phoneandpad.com")) {
						Intent intent = getPackageManager()
								.getLaunchIntentForPackage(
										"mobileeye_phoneandpad.com");
						if (intent != null) {
							// We found the activity now start the activity
							intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(intent);
						}

					} else {
						startActivity(new Intent(
								Intent.ACTION_VIEW,
								Uri.parse("market://details?id=mobileeye_phoneandpad.com")));
					}
					Log.d("LOGIN::", "Clicked");
					// Toast.makeText(MainActivity.this,
					// "Watch Live Button  clicked", Toast.LENGTH_LONG).show();
				}
			}, "valid");

			Log.i("Load=", "URL=" + url);
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {

			super.onPageStarted(view, url, favicon);
			if (splashImageView.getVisibility() == View.VISIBLE)
				progressBar.setVisibility(View.GONE);
			else
				progressBar.setVisibility(View.VISIBLE);

		}

		@Override
		public void onPageFinished(WebView view, String url) {

			super.onPageFinished(view, url);

			String username = "", password = "";
			
			Log.i("username=", username);
			Log.i("password=", password);

			splashImageView.setVisibility(View.GONE);
			progressBar.setVisibility(View.GONE);

		}

	}

	class MyJavaScriptInterface {

		private Context ctx;

		MyJavaScriptInterface(Context ctx) {
			this.ctx = ctx;
		}

		@JavascriptInterface
		public void showHTML(String html) {
			new AlertDialog.Builder(ctx).setTitle("HTML").setMessage(html)
					.setPositiveButton(android.R.string.ok, null)
					.setCancelable(false).create().show();
		}

	}

	public boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) MainActivity_daulat_original.this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();

			if (info != null)

				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
						return true;

		}
		return false;
	}

	public void alert() {
		Builder alert1 = new AlertDialog.Builder(this);
		alert1.setMessage(getString(R.string.msg))
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						MainActivity_daulat_original.this.finish();
					}
				}).setCancelable(false).show();

	}

	@Override
	public void onBackPressed() {
		// Toast.makeText(getApplicationContext(), "Hello="+prevURl,
		// Toast.LENGTH_SHORT).show();
		Log.i("prevURl=", "prevURl=" + prevURl);

		if (prevURl != null && !prevURl.contains("login.html") ) {
			//webViewBrowser.loadUrl(prevURl);
			if (webViewBrowser.canGoBack()) {
				webViewBrowser.goBack();
		    } else {
		        super.onBackPressed();
		    }
		}

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		super.onConfigurationChanged(newConfig);
		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {

			((ImageView) findViewById(R.id.splashImageView))
					.setImageResource(R.drawable.kul_splash);

		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			((ImageView) findViewById(R.id.splashImageView))
					.setImageResource(R.drawable.kul_splash);

		}
	}

	private boolean isAppInstalled(String packageName) {
		PackageManager pm = getPackageManager();
		boolean installed = false;
		try {
			pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
			installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			installed = false;
		}
		return installed;
	}

	private Intent newEmailIntent(Context context, String address,
			String subject, String body, String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}
}
