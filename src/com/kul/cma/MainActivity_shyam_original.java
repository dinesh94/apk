package com.kul.cma;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.test.R;


@SuppressLint("SetJavaScriptEnabled")
public class MainActivity_shyam_original extends Activity implements OnClickListener {

	private WebView webViewBrowser;
	private ImageView splashImageView;
	private View progressBar;
    Button loginbutton;
	
    private ValueCallback<Uri> mUploadMessage;  
    private final static int FILECHOOSER_RESULTCODE=1;  
      
    @Override  
    protected void onActivityResult(int requestCode, int resultCode,  
                                       Intent intent) {  
     if(requestCode==FILECHOOSER_RESULTCODE)  
     {  
      if (null == mUploadMessage) return;  
               Uri result = intent == null || resultCode != RESULT_OK ? null  
                       : intent.getData();  
               mUploadMessage.onReceiveValue(result);  
               mUploadMessage = null;  
                 
     }  
    }  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
		webViewBrowser = (WebView) findViewById(R.id.webView);
		splashImageView = (ImageView) findViewById(R.id.splashImageView);
		loginbutton = new Button(MainActivity_shyam_original.this);
		loginbutton.setOnClickListener(this);
		progressBar = findViewById(R.id.progressBar);
		webViewBrowser.setWebViewClient(new MyBrowser());
		webViewBrowser.setWebChromeClient(new WebChromeClient());
		webViewBrowser.getSettings().setLoadsImagesAutomatically(true);
		webViewBrowser.getSettings().setJavaScriptEnabled(true);
		webViewBrowser.getSettings().setSupportMultipleWindows(true);
		//webViewBrowser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webViewBrowser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	//	webViewBrowser.addJavascriptInterface(new MyJavaScriptInterface(MainActivity.this), "HtmlViewer");
		//webViewBrowser.addJavascriptInterface(new WebAppInterface(MainActivity.this), "Android");
		webViewBrowser.getSettings().setDomStorageEnabled(true);
		webViewBrowser.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

		
		
        // Enablejavascript
		WebSettings ws = webViewBrowser.getSettings();
		ws.setJavaScriptEnabled(true);
		// Add the interface to record javascript events
		webViewBrowser.addJavascriptInterface(loginbutton, "loginbutton");
		//webViewBrowser.addJavascriptInterface(refuse, "refuse");
		
		if (isConnectingToInternet())
			webViewBrowser.loadUrl(getString(R.string.load_url));
		else
			alert();
		
		  webViewBrowser.setWebChromeClient(new WebChromeClient()  
		    {  

		         
		    	@Override
		    	public boolean onCreateWindow(WebView view, boolean isDialog,
		    			boolean isUserGesture, Message resultMsg) {
		             WebView newWebView = new WebView(view.getContext());

		             WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
		             transport.setWebView(newWebView);
		             resultMsg.sendToTarget();
		             
		             
		             Log.i("HH-","In="+view.getUrl());
		             return true;
		         }
		    	
		    	/* multiple webview window overload method - start 
		         public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg)
		         {
		             WebView newWebView = new WebView(view.getContext());

		             WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
		             transport.setWebView(newWebView);
		             resultMsg.sendToTarget();
		             return true;
		         }*/

		         /* multiple webview window overload method - end */
		    });  
	/*	
		webViewBrowser.setWebChromeClient(new WebChromeClient()  
		  {  
		         //The undocumented magic method override  
		         //Eclipse will swear at you if you try to put @Override here  
		         public void openFileChooser(ValueCallback<Uri> uploadMsg) {  
		           
		          mUploadMessage = uploadMsg;  
		          Intent i = new Intent(Intent.ACTION_GET_CONTENT);  
		          i.addCategory(Intent.CATEGORY_OPENABLE);  
		          i.setType("image/*");  
		          MainActivity.this.startActivityForResult(Intent.createChooser(i,"File Chooser"), FILECHOOSER_RESULTCODE);  
		     
		         }  
		  });  */

	}
	@Override
	public void onClick(View v) {
	    if (v.equals(loginbutton)) {
	     Toast.makeText(getApplicationContext(), "Login",Toast.LENGTH_LONG).show();
	    } 
	    
	    Toast.makeText(getApplicationContext(), "Login",Toast.LENGTH_LONG).show();
		
	}
	
	public class WebAppInterface {
	    Context mContext;

	    /** Instantiate the interface and set the context */
	    WebAppInterface(Context c) {
	        mContext = c;
	    }

	    /** Show a toast from the web page */
	    @JavascriptInterface
	    public void showToast(String toast) {
	        Toast.makeText(mContext, "Hello="+toast, Toast.LENGTH_SHORT).show();
	    }
	}
	private class MyBrowser extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {/*
			
			 if (Uri.parse(url).getScheme().equals("market")) {
		            try {
		                Intent intent = new Intent(Intent.ACTION_VIEW);
		                intent.setData(Uri.parse(url));
		                Activity host = (Activity) view.getContext();
		                host.startActivity(intent);
		                return true;
		            } catch (ActivityNotFoundException e) {
		                // Google Play app is not installed, you may want to open the app store link
		                Uri uri = Uri.parse(url);
		                view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
		                return false;
		            }
			 }

			view.loadUrl("javascript:window.Android.showToast(javascript:document.getElementById('LoginUser_Username').value);");
			String username="",password="";
			view.loadUrl("javascript:window.HtmlViewer.showHTML" +
                       "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
			view.loadUrl("javascript:document.getElementById('LoginUser.Username').value='"+username+"';javascript:document.getElementById('LoginUser.Password').value = '"+password+"';");
	view.loadUrl("javascript:window.Android.showToast(document.getElementById('LoginUser_Username').value);");
	view.loadUrl("javascript:window.Android.showToast(document.getElementById('LoginUser_Password').value);");

			Log.i("username=",username);
			Log.i("password=",password);
			
			Log.i("Shyam=",url);
			
			
			
			//Log.i("Shyam=",view.get);
			 view.addJavascriptInterface(new Object()
	          {
	              @JavascriptInterface
	            public void performClick() throws Exception
	            {
	                Log.d("LOGIN::", "Clicked");
	                 Toast.makeText(MainActivity.this, "Login clicked", Toast.LENGTH_LONG).show();
	            }
	          }, "Login");
			if (url.startsWith("mailto:")) {
			         MailTo mt = MailTo.parse(url);
			        Intent i = newEmailIntent(MainActivity.this, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
			        startActivity(i);
			        //view.reload();
			        return true;
			      
			    } 
			else if (url.startsWith("tel:")) { 
	                Intent intent = new Intent(Intent.ACTION_DIAL,
	                        Uri.parse(url)); 
	                startActivity(intent); 
	                return true;
	        }
			else
			{
			view.loadUrl(url);
			return true;
			}
			
		*/	
			
			Log.i("Load=", "URL="+url);
			view.loadUrl(url);
			return false;
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {

			super.onPageStarted(view, url, favicon);
			if (splashImageView.getVisibility() == View.VISIBLE)
				progressBar.setVisibility(View.GONE);
			else
				progressBar.setVisibility(View.VISIBLE);

		}
		
		
		
		@Override
		public void onPageFinished(WebView view, String url) {

			super.onPageFinished(view, url);
		
			//  view.loadUrl("javascript:document.forms[0].q.value='[android]'"); 
				String username="",password="";
			/*view.loadUrl("javascript:window.HtmlViewer.showHTML" +
                       "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
	*/		//view.loadUrl("javascript:document.getElementById('loginbutton').value='"+username+"';javascript:document.getElementById('LoginUser.Password').value = '"+password+"';");

			Log.i("username=",username);
			Log.i("password=",password);

			splashImageView.setVisibility(View.GONE);
			progressBar.setVisibility(View.GONE);

		}
		
		
		
		

	}
	class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }
        @JavascriptInterface 
        public void showHTML(String html) {
            new AlertDialog.Builder(ctx).setTitle("HTML").setMessage(html)
                    .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();
        }

    }
	public boolean isConnectingToInternet() {
		ConnectivityManager connectivity = (ConnectivityManager) MainActivity_shyam_original.this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();

			if (info != null)

				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
						return true;

		}
		return false;
	}

	
	public void alert() {
		Builder alert1 = new AlertDialog.Builder(this);
		alert1.setMessage(getString(R.string.msg))
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						MainActivity_shyam_original.this.finish();
					}
				}).setCancelable(false).show();

	}
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
		.setTitle("Exit")
		.setMessage("Do you want to exit?")
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) { 
				finish();
								
			}
		})

		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		})
		
		.show();
		
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		
		super.onConfigurationChanged(newConfig);
		 // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	
	    	((ImageView)findViewById(R.id.splashImageView)).setImageResource(R.drawable.kul_splash);
	   
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	      	((ImageView)findViewById(R.id.splashImageView)).setImageResource(R.drawable.kul_splash);
	 	   

	    }
	}
	private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
	    Intent intent = new Intent(Intent.ACTION_SEND);
	    intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
	    intent.putExtra(Intent.EXTRA_TEXT, body);
	    intent.putExtra(Intent.EXTRA_SUBJECT, subject);
	    intent.putExtra(Intent.EXTRA_CC, cc);
	    intent.setType("message/rfc822");
	    return intent;
	  }
}
